## Lily KiCad Projects

This repository contains multiple KiCad projects for personal or commercial use.
All projects apply to the following guidelines:

* New projects are created with the latest stable release of KiCad
* When a new major version of KiCad is released, the project is converted to the newest version
* A maximum of two versions are maintained: the latest stable version and the version before that
* Only the standard KiCad libraries are used to ceate all the projects

In case of any issues, you can post an issue through the ussues board: https://gitlab.com/lily-projects/lily-kicad-projects/-/issues

The projects are delivered as is. We are not responsible or liable for any damage caused by using the information from these projects.

2023 - LilyTronics.nl (https://lilytronics.nl)
